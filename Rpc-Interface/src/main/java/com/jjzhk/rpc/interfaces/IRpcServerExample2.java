package com.jjzhk.rpc.interfaces;

/**
 * IRpcServerExample2
 *
 * @author : JJZHK
 * @date : 2016-08-15
 * @comments :
 **/
public interface IRpcServerExample2 {
    String hello(String name);
}
