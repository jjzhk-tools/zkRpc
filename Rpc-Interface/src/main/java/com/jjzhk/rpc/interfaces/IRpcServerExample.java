package com.jjzhk.rpc.interfaces;

/**
 * RpcServerExample
 *
 * @author : JJZHK
 * @date : 2016-08-13
 * @comments :
 **/
public interface IRpcServerExample {
    String hello(String name);
}
