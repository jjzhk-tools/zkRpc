package com.jjzhk.rpc.myutils;/**
 * HessianTest
 *
 * @author : JJZHK
 * @date : 2016-09-03
 * @comments :
 **/

import com.jjzhk.common.bean.RpcRequest;
import com.jjzhk.common.utils.SerializationUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HessianTest {
    @Test
    public void test01()
    {
        RpcRequest request = new RpcRequest();
        request.setRequestID("1");
        request.setInterfaceName("com.jjzhk");
        request.setMethodName("helloWorld");
        request.setServiceVersion("aaa");
        request.setParameterTypes(new Class[]{String.class});
        request.setParameterValues(new Object[]{"123"});

        SerializationUtils utils = new HessionSerializationUtils();
        byte[] bytes = utils.serialize(request);

        RpcRequest test = utils.deserialize(bytes, RpcRequest.class);
        System.out.println(test.getInterfaceName());
    }
}
