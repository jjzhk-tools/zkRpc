![Images](https://img.shields.io/badge/RPC-%E9%A1%B9%E7%9B%AE%E5%AE%8C%E7%BB%93-9932CC?style=plastic&logo=appveyor)
### 服务器端:
    <puddo:application port="9092"/>
    <puddo:service package="com.jjzhk.rpc.impl"/>
    <puddo:registry address="192.168.3.23" port="2181"/>

    需要暴露的服务,使用@PuddoService注解进行修饰

### 客户端:

    <puddo:application port="9092"/>
    <puddo:registry address="192.168.3.23" port="2181"/>
    <puddo:caller interface="com.jjzhk.rpc.interfaces.IRpcServerExample"
                  ref="rpcServerExample" interval="1000" trycount="2"/>
    <puddo:caller interface="com.jjzhk.rpc.interfaces.IRpcServerExample"
                  ref="rpcServerExampleEx" interval="3000" trycount="4"/>
    <puddo:caller interface="com.jjzhk.rpc.interfaces.IRpcServerExample2"
                  ref="rpcServerExample02" interval="5000" trycount="10"/>

ApplicationContext context = new ClassPathXmlApplicationContext("client.xml");

IRpcServerExample helloService = (IRpcServerExample)context.getBean("rpcServerExample");
String result = helloService.hello("World");
System.out.println(result);

具体参考Rpc-Server和Rpc-Client

主要用法类似于dubbo

# 捐助
<img src='https://images.gitee.com/uploads/images/2020/0909/153732_ba1bfd06_956405.png' height='450' width='750'/>