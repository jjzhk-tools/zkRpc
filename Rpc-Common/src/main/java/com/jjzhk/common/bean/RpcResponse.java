package com.jjzhk.common.bean;

import java.io.Serializable;

/**
 * RpcResponse
 *
 * @author : JJZHK
 * @date : 2016-08-14
 * @comments :
 **/
public class RpcResponse implements Serializable{
    private String requestID;
    private Exception exception;
    private Object result;

    public boolean hasException()
    {
        return exception != null;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
