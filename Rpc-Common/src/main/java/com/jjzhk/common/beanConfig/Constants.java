package com.jjzhk.common.beanConfig;

/**
 * Constants
 *
 * @author : JJZHK
 * @date : 2016-08-17
 * @comments :
 **/
public interface Constants {
    String ZK_PATH = "/puddo";
    int ZK_SESSION_TIMEOUT = 5000;
    int ZK_CONNECTION_TIMEOUT = 1000;
}
