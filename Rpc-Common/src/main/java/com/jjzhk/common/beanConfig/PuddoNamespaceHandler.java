package com.jjzhk.common.beanConfig;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * PuddoNamespaceHandler
 *
 * @author : JJZHK
 * @date : 2016-08-15
 * @comments :
 **/
public class PuddoNamespaceHandler extends NamespaceHandlerSupport{
    @Override
    public void init() {
        this.registerBeanDefinitionParser("application", new PuddoBeanDefinitionParser(ApplicationBean.class));
        this.registerBeanDefinitionParser("service", new PuddoBeanDefinitionParser(ServiceBean.class));
        this.registerBeanDefinitionParser("caller", new PuddoBeanDefinitionParser(CallerBean.class));
        this.registerBeanDefinitionParser("registry", new PuddoBeanDefinitionParser(RegistryBean.class));
    }
}
