package com.jjzhk.common;

import org.springframework.stereotype.Component;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.UUID;

/**
 * PuddoService
 *
 * @author : JJZHK
 * @date : 2016-08-13
 * @comments :
 **/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface PuddoService {
    Class<?> value();
    String uuid();
}