package com.jjzhk.common.handler;

import com.jjzhk.common.bean.RpcRequest;
import com.jjzhk.common.utils.ReflexUtils;
import com.jjzhk.common.utils.SerializationUtils;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * RpcEncoder
 *
 * @author : JJZHK
 * @date : 2016-08-14
 * @comments :
 **/
public class RpcEncoder extends MessageToByteEncoder{
    private Class<?> genericClass;
    private Class<?> forSerialization;

    public RpcEncoder(Class<?> genericClass, Class<?> forSerialization) {
        this.genericClass = genericClass;
        this.forSerialization = forSerialization;
    }


    @Override
    protected void encode(ChannelHandlerContext ctx, Object in, ByteBuf out) throws Exception {
        if (genericClass.isInstance(in))
        {
            SerializationUtils obj = ReflexUtils.newObject((Class<SerializationUtils>) this.forSerialization);

            byte[] data = obj.serialize(in);
            out.writeInt(data.length);
            out.writeBytes(data);
        }
    }
}
