package com.jjzhk.common.utils;

import io.protostuff.LinkedBuffer;
import io.protostuff.ProtostuffIOUtil;
import io.protostuff.Schema;
import io.protostuff.runtime.RuntimeSchema;
import org.objenesis.Objenesis;
import org.objenesis.ObjenesisStd;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * SerializationUtils
 *
 * @author : JJZHK
 * @date : 2016-08-13
 * @comments :
 **/
public interface SerializationUtils {
    <T> byte[] serialize(T obj);
    <T> T deserialize(byte[] data, Class<T> cls);
}