package com.jjzhk.rpc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * rpcServer
 *
 * @author : JJZHK
 * @date : 2016-08-16
 * @comments :
 **/
public class rpcServer {
    public static void main(String[] args){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("server.xml");
    }
}
