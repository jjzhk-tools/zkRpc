package com.jjzhk.rpc.impl;

import com.jjzhk.common.PuddoService;
import com.jjzhk.rpc.interfaces.IRpcServerExample;

/**
 * RpcServerExample
 *
 * @author : JJZHK
 * @date : 2016-08-13
 * @comments :
 **/
@PuddoService(value = IRpcServerExample.class, uuid = "rpcServerExample")
public class RpcServerExample implements IRpcServerExample {
    @Override
    public String hello(String name) {
        return "hello, " + name;
    }
}
