package com.jjzhk.rpc.impl;

import com.jjzhk.common.PuddoService;
import com.jjzhk.rpc.interfaces.IRpcServerExample;

/**
 * RpcServerExampleEx
 *
 * @author : JJZHK
 * @date : 2016-08-15
 * @comments :
 **/
@PuddoService(value = IRpcServerExample.class, uuid = "rpcServerExampleEx")
public class RpcServerExampleEx implements IRpcServerExample {
    @Override
    public String hello(String name) {
        return "hello, " + name;
    }
}