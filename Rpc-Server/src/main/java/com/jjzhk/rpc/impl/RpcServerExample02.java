package com.jjzhk.rpc.impl;

import com.jjzhk.common.PuddoService;
import com.jjzhk.rpc.interfaces.IRpcServerExample2;

/**
 * RpcServerExample02
 *
 * @author : JJZHK
 * @date : 2016-08-15
 * @comments :
 **/
@PuddoService( value = IRpcServerExample2.class, uuid = "rpcServerExample02")
public class RpcServerExample02 implements IRpcServerExample2 {
    @Override
    public String hello(String name) {
        return "hello, " + name;
    }
}