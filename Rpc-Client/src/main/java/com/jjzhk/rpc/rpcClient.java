package com.jjzhk.rpc;

import com.jjzhk.rpc.interfaces.IRpcServerExample;
import com.jjzhk.rpc.interfaces.IRpcServerExample2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.concurrent.CountDownLatch;

/**
 * rpcClient
 *
 * @author : JJZHK
 * @date : 2016-08-16
 * @comments :
 **/
public class rpcClient {
    public static void main(String[] args){
        ApplicationContext context = new ClassPathXmlApplicationContext("client.xml");

        IRpcServerExample helloService = (IRpcServerExample)context.getBean("rpcServerExample");
        CountDownLatch countDownLatch = new CountDownLatch(1);
        for (int i = 0; i <1; i++)
        {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String result = helloService.hello("World, rpcServerExample");
                    System.out.println(result);
                }
            }).start();
        }
//        try {
//            countDownLatch.await();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

//        helloService = (IRpcServerExample)context.getBean("rpcServerExampleEx");
//        result = helloService.hello("World, rpcServerExampleEx");
//        System.out.println(result);
//
//        IRpcServerExample2 helloService2 = (IRpcServerExample2)context.getBean("rpcServerExample02");
//        result = helloService2.hello("World, rpcServerExample02");
//        System.out.println(result);
    }
}
